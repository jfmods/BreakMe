package io.gitlab.jfronny.breakme.client;

import io.gitlab.jfronny.commons.throwable.Try;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.thread.ThreadExecutor;

public interface Client {
    Client INSTANCE = (Client) Try.orThrow(() -> Class.forName(Client.class.getName() + "Impl").getConstructor().newInstance());

    ThreadExecutor<Runnable> getRunner();
    boolean isValidPlayer(PlayerEntity player);
}
