package io.gitlab.jfronny.breakme.mixin;

import io.gitlab.jfronny.breakme.BreakMe;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
public class LivingEntityMixin {
    // client-side damage event when playing on a server
    @Inject(at = @At("TAIL"), method = "onDamaged(Lnet/minecraft/entity/damage/DamageSource;)V")
    private void onDamage(DamageSource damageSource, CallbackInfo ci) throws Exception {
        if (((LivingEntity)(Object)this) instanceof PlayerEntity player) {
            BreakMe.tryInvokeCrash(BreakMe.resolveEvent(player));
        }
    }
}
