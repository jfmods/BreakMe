package io.gitlab.jfronny.breakme.mixin;

import io.gitlab.jfronny.breakme.BreakMe;
import io.gitlab.jfronny.breakme.BreakMeConfig;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public class PlayerEntityMixin {
    @Inject(at = @At("TAIL"), method = "damage(Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/entity/damage/DamageSource;F)Z")
    private void onDamage(ServerWorld world, DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) throws Exception {
        if (cir.getReturnValue()) {
            BreakMe.tryInvokeCrash(BreakMe.resolveEvent((PlayerEntity)(Object)this));
        }
    }

    @Inject(at = @At("TAIL"), method = "onDeath(Lnet/minecraft/entity/damage/DamageSource;)V")
    private void onDeath(DamageSource damageSource, CallbackInfo ci) throws Exception {
        if (BreakMe.isValidPlayer((PlayerEntity)(Object)this)) {
            BreakMe.tryInvokeCrash(BreakMeConfig.Cause.Death);
        }
    }
}
