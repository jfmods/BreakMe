package io.gitlab.jfronny.breakme;

import io.gitlab.jfronny.breakme.crash.Method;
import io.gitlab.jfronny.libjf.config.api.v2.*;

@JfConfig
public class BreakMeConfig {
    @Entry public static Cause event = Cause.Death;
    @Entry public static Method method = Method.Segfault;

    public enum Cause {
        Damage,
        Death,

        All,
        None;

        public boolean includes(Cause cause) {
            if (cause == null || cause == None) return false;
            return this == All || this == cause;
        }
    }

    @Verifier
    public static void validProvider() {
        if (BreakMeConfig.method == null) {
            BreakMeConfig.method = Method.None;
            BreakMe.LOGGER.error("Could not find specified crash provider, defaulting to None");
        }
    }

    @Verifier
    public static void validEvent() {
        if (BreakMeConfig.event == null) {
            BreakMeConfig.event = Cause.None;
            BreakMe.LOGGER.error("Could not find specified event, defaulting to None");
        }
    }

    static {
        JFC_BreakMeConfig.ensureInitialized();
    }
}
