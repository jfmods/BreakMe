package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.client.Client;
import io.gitlab.jfronny.breakme.crash.CrashProvider;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;

public class HangProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) {
            Client.INSTANCE.getRunner().send(this::hang);
        }
        hang();
    }

    private void hang() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
