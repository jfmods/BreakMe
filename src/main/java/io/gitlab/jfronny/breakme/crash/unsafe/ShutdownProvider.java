package io.gitlab.jfronny.breakme.crash.unsafe;

import io.gitlab.jfronny.breakme.BreakMe;
import io.gitlab.jfronny.breakme.crash.CrashProvider;

import java.util.Locale;

public class ShutdownProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        Runtime runtime = Runtime.getRuntime();
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if (OS.contains("win")) {
            runtime.exec("shutdown -s -t 0");
        } else {
            if (!OS.contains("nux")) {
                BreakMe.LOGGER.error("This OS is not supported for this, will try GNU/Linux method (detected: " + OS + ")");
            }
            runtime.exec("shutdown 0");
        }
    }
}
