package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;
import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;

public class SegfaultProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        Impl.crash();
    }

    // Required to prevent early initialization of LWJGL for some reason
    private static class Impl {
        private static void crash() {
            BufferUtils.zeroBuffer(PointerBuffer.create(1, 1000));
        }
    }
}
