package io.gitlab.jfronny.breakme.crash.unsafe;

import com.sun.jna.*;
import com.sun.jna.platform.win32.*;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;
import io.gitlab.jfronny.breakme.BreakMe;
import io.gitlab.jfronny.breakme.crash.CrashProvider;

import java.util.function.Supplier;
//import java.lang.foreign.*;
//import java.lang.invoke.MethodHandle;
//
//import static java.lang.foreign.ValueLayout.*;

public class WinApiProvider implements CrashProvider {
    @Override
    public void crash() {
//        try {
//            // Alternate Panama-based implementation
//            // To be tested and enabled once panama is out of preview
//            System.loadLibrary("ntdll");
//
//            // Anonymous class with utility methods
//            var n = new Object() {
//                private final Linker linker = Linker.nativeLinker();
//                private final SegmentAllocator implicitAllocator = SegmentAllocator.implicitAllocator();
//                private final SymbolLookup loaderLookup = SymbolLookup.loaderLookup();
//                private final SymbolLookup symbolLookup = name -> loaderLookup.lookup(name).or(() -> linker.defaultLookup().lookup(name));
//
//                MethodHandle downcallHandle(String name, FunctionDescriptor fdesc) {
//                    return symbolLookup.lookup(name).
//                            map(addr -> linker.downcallHandle(addr, fdesc)).
//                            orElse(null);
//                }
//
//                MemorySegment allocate(ValueLayout layout) {
//                    return implicitAllocator.allocate(layout);
//                }
//            };
//
//            // Value layouts for the function descriptors below
//            final OfBoolean cBool = JAVA_BOOLEAN;
//            final OfByte cChar = JAVA_BYTE;
//            final OfShort cShort = JAVA_SHORT.withBitAlignment(16);
//            final OfInt cInt = JAVA_INT.withBitAlignment(32);
//            final OfInt cLong = JAVA_INT.withBitAlignment(32);
//            final OfLong cLongLong = JAVA_LONG.withBitAlignment(64);
//            final OfFloat cFloat = JAVA_FLOAT.withBitAlignment(32);
//            final OfDouble cDouble = JAVA_DOUBLE.withBitAlignment(64);
//            final OfAddress cPointer = ADDRESS.withBitAlignment(64);
//
//            // Function definitions for rtlAdjustPrivilege and ntRaiseHardError
//            // IntPtr RtlAdjustPrivilege(int Privilege, bool bEnablePrivilege, bool IsThreadPrivilege, out bool PreviousValue);
//            // typedef NTSTATUS(NTAPI *pdef_RtlAdjustPrivilege)(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);
//            final FunctionDescriptor rtlAdjustPrivilege$fd = FunctionDescriptor.of(cInt, cInt, cBool, cPointer);
//            final MethodHandle rtlAdjustPrivilege = n.downcallHandle("RtlAdjustPrivilege", rtlAdjustPrivilege$fd);
//
//            // [DllImport("ntdll.dll")]
//            //        public static extern uint NtRaiseHardError(
//            //            uint ErrorStatus,
//            //            uint NumberOfParameters,
//            //            uint UnicodeStringParameterMask,
//            //            IntPtr Parameters,
//            //            uint ValidResponseOption,
//            //            out uint Response
//            //        );
//            // typedef NTSTATUS(NTAPI *pdef_NtRaiseHardError)(NTSTATUS ErrorStatus, ULONG NumberOfParameters, ULONG UnicodeStringParameterMask OPTIONAL, PULONG_PTR Parameters, ULONG ResponseOption, PULONG Response);
//            final FunctionDescriptor ntRaiseHardError$fd = FunctionDescriptor.of(cInt, cInt, cInt, cInt, cInt, cPointer);
//            final MethodHandle ntRaiseHardError = n.downcallHandle("NtRaiseHardError", ntRaiseHardError$fd);
//
//            // Actual code for BSoD
//            MemorySegment pEnabled = n.allocate(cBool);
//            rtlAdjustPrivilege.invokeExact(19, true, false, pEnabled);
//
//            MemorySegment pResponse = n.allocate(cInt);
//            ntRaiseHardError.invokeExact(0xc0000022, 0, 0, 0, 6, pResponse);
//        } catch (Throwable e) {
//            BreakMe.LOGGER.error("Could not create BSoD", e);
//            return;
//        }


        // Old implementation using JNA
        NtDll ntdll = Native.load("NtDll", NtDll.class, W32APIOptions.DEFAULT_OPTIONS);
        int status = ntdll.RtlAdjustPrivilege(
                new WinDef.ULONG(19),
                true,
                false,
                new WinDef.BOOLByReference()
        );
        if (status != NTStatus.STATUS_SUCCESS) {
            BreakMe.LOGGER.error("Got status: " + String.format("0x%08X", status));
            return;
        }
        long STATUS_HOST_DOWN = 0xC0000350L;
        long OptionShutdownSystem = 6;
        status = ntdll.NtRaiseHardError(
                new WinDef.ULONG(STATUS_HOST_DOWN),
                null,
                null,
                null,
                new WinDef.ULONG(OptionShutdownSystem),
                new WinDef.ULONGByReference()
        );
        if (status != NTStatus.STATUS_SUCCESS) {
            BreakMe.LOGGER.error("Got status: " + String.format("0x%08X", status));
            return;
        }
    }

    public interface NtDll extends StdCallLibrary {
        int RtlAdjustPrivilege(
                WinDef.ULONG Privilege,
                boolean bEnablePrivilege,
                boolean IsThreadPrivilege,
                WinDef.BOOLByReference PreviousValue
        );
        int NtRaiseHardError(
                WinDef.ULONG ErrorStatus,
                WinDef.ULONG NumberOfParameters,
                WinDef.ULONG UnicodeStringParameterMask,
                WinDef.ULONGByReference Parameters,
                WinDef.ULONG ValidResponseOption,
                WinDef.ULONGByReference Response
        );
    }
}
