package io.gitlab.jfronny.breakme.crash;

public interface CrashProvider {
    void crash() throws Exception;
}
