package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

public class ExitCodeProvider implements CrashProvider {
    @Override
    public void crash() {
        System.exit(-1);
    }
}
