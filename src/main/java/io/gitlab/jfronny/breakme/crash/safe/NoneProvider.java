package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

public class NoneProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {

    }
}
