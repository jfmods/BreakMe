package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

public class SecurityExceptionProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        throw new SecurityException("You did bad, now die");
    }
}
