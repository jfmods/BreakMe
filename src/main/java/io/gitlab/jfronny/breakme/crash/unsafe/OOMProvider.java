package io.gitlab.jfronny.breakme.crash.unsafe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public class OOMProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        List<ByteBuffer> bl = new LinkedList<>();
        while (true) bl.add(ByteBuffer.allocate(1024 * 1024 * 1024));
    }
}
