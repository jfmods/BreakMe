package io.gitlab.jfronny.breakme.crash.unsafe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

public class ForkbombProvider implements CrashProvider {
    @Override
    public void crash() {
        forkbomb.main(new String[0]);
    }
}
