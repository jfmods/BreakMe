package io.gitlab.jfronny.breakme.crash.safe;

import io.gitlab.jfronny.breakme.crash.CrashProvider;

public class ExceptionProvider implements CrashProvider {
    @Override
    public void crash() throws Exception {
        throw new Exception("You did bad, now die");
    }
}
