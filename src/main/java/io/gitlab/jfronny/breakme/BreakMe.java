package io.gitlab.jfronny.breakme;

import io.gitlab.jfronny.breakme.client.Client;
import io.gitlab.jfronny.breakme.crash.Method;
import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Language;

public class BreakMe implements ModInitializer {
    public static final String MOD_ID = "breakme";
    public static final SystemLoggerPlus LOGGER = SystemLoggerPlus.forName(MOD_ID);

    @Override
    public void onInitialize() {
        LOGGER.warn("Prepare for trouble");
    }

    public static void tryInvokeCrash(BreakMeConfig.Cause cause) throws Exception {
        if (BreakMeConfig.event.includes(cause)) {
            crash();
        }
    }

    public static BreakMeConfig.Cause resolveEvent(PlayerEntity player) {
        if (!isValidPlayer(player)) return BreakMeConfig.Cause.None;
        else if (player.isDead()) return BreakMeConfig.Cause.Death;
        else return BreakMeConfig.Cause.Damage;
    }

    public static boolean isValidPlayer(PlayerEntity player) {
        if (player == null) return false;
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) {
            return Client.INSTANCE.isValidPlayer(player);
        }
        return true;
    }

    private static void crash() throws Exception {
        Method method = BreakMeConfig.method;
        String name = Language.getInstance().get(MOD_ID + ".jfconfig.enum.Method." + method.name(), method.name());
        LOGGER.info("Invoking the crash (using {0})", name);
        method.crash();
    }
}
