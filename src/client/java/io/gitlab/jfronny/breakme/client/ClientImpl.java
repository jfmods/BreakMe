package io.gitlab.jfronny.breakme.client;

import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.thread.ThreadExecutor;

import java.util.Objects;

public class ClientImpl implements Client {
    @Override
    public ThreadExecutor<Runnable> getRunner() {
        return Objects.requireNonNull(MinecraftClient.getInstance());
    }

    @Override
    public boolean isValidPlayer(PlayerEntity player) {
        MinecraftClient client = MinecraftClient.getInstance();
        if (client == null || client.player == null) return false;
        return client.player.getUuid().equals(player.getUuid());
    }
}
