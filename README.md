BreakMe allows configuring an event (death/taking damage) that causes minecraft to crash. It can also be configured to shut down or crash the whole OS.
It uses Fabric (although porting should be trivial, most of the code is standard java & some JNI)

Please note that shutting down (or worse: crashing) an OS while things are still open might cause data loss,
I am not responsible for that if you decide to enable that setting

# Configuration
You can use [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu) to change the configuration.\
The values are explained in more detail below

- Cause:
    - Damage: The crash-event is fired once you take damage
    - Death: The crash-event is fired once you die
    - All: The crash-event is fired if any of the above apply (currently equal to Damage)
    - None: The crash-event is never fired

- Method:
    - Unsafe_Universal_Forkbomb: Launch a self-multiplying process
    - Unsafe_Windows_WinAPI: Do some JNI-magic to instantly produce a blue-screen on windows
    - Unsafe_Universal_OOM: Causes an OOM exception
    - Broken_Universal_ExitCode: Exit the integrated Server. The game still displays, but you can no longer interact with the world or quit.
    - Safe_Universal_Hang: Hang both the client and integrated server thread.
    - Safe_Universal_Exception: Performs an invalid operation. Behaves like every other crash and just closes the game, leaving a crash log.
    - SemiUnsafe_Universal_Exception: Throws a security exceptions. This does not work properly for 1.16.2
    - SemiUnsafe_Universal_Shutdown: Attempts to run a shutdown command. Since these are specific to some systems this might not always work.
    - SemiUnsafe_Universal_Segfault: Causes a segmentation fault using lwjgl.
    - SemiUnsafe_Universal_StackOverflow: Causes a stack overflow via infinite recursion.
    - None: Do nothing

Please note that all methods marked "Unsafe" as well as the shutdown method are not available in the CurseForge release

<!-- modrinth_exclude.start -->
# Building
A prebuilt DLL is included in the source tree and will be removed once it is replaced by a panama implementation.\
If you want to build it yourself, use src/main/c/build.bat to build it on a windows PC.\
The mod itself is built like any other fabric mod: using gradle
<!-- modrinth_exclude.end -->
