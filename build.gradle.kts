import com.squareup.javapoet.ClassName
import com.squareup.javapoet.ParameterizedTypeName
import com.squareup.javapoet.TypeSpec
import io.gitlab.jfronny.scripts.*
import java.nio.file.Files
import java.util.*
import java.util.function.Supplier
import javax.lang.model.element.Modifier.*

plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "breakme"

// https://fabricmc.net/develop/
jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "breakme"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }
    curseforge {
        projectId = "400842"
        requiredDependencies.add("libjf")
        optionalDependencies.add("modmenu")
    }
}

if (flavour == "") flavour = "modrinth"

dependencies {
    modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v2")

    // Dev env
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-ui-tiny")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-api")
    modLocalRuntime("com.terraformersmc:modmenu:12.0.0-beta.1")
}

loom {
    runs {
        this.named("client").get().vmArg("-Xmx2G")
    }
}

fun list(`package`: String): List<ClassName> = Files.list(projectDir.resolve("src/main/java").resolve(`package`.replace('.', '/')).toPath()).use { stream ->
    stream
        .map { it.fileName.toString() }
        .map { it.substring(0, it.lastIndexOf('.')) }
        .filter { it.endsWith("Provider") }
        .map { ClassName.get(`package`, it) }
        .toList()
}

val classes = LinkedList(list("io.gitlab.jfronny.breakme.crash.safe"))

if (flavour == "curseforge") {
    sourceSets.main.get().java.filter.exclude("**/unsafe/*")
} else {
    classes.addAll(list("io.gitlab.jfronny.breakme.crash.unsafe"))
}

sourceSets {
    main {
        generate(project) {
            val providerType = ClassName.get("io.gitlab.jfronny.breakme.crash", "CrashProvider")
            val supplierType = ParameterizedTypeName.get(ClassName.get(Supplier::class.java), providerType)

            `enum`("io.gitlab.jfronny.breakme.crash", "Method") {
                modifiers(PUBLIC)

                superInterface(providerType)

                for (klazz in classes) {
                    val name = klazz.simpleName()
                    enumConstant(name.substring(0, name.length - "Provider".length), TypeSpec.anonymousClassBuilder("\$T::new", klazz).build())
                }

                field(supplierType, "provider", PRIVATE, FINAL)

                constructor {
                    parameter(supplierType, "provider")
                    code {
                        addStatement("this.provider = new \$T<>(provider)", ClassName.get("io.gitlab.jfronny.commons", "LazySupplier"))
                    }
                }

                method("crash") {
                    modifiers(PUBLIC)
                    exception(Exception::class.java)
                    annotation(Override::class.java)
                    code {
                        addStatement("provider.get().crash()")
                    }
                }
            }
        }
    }
}
